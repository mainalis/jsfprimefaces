package com.ui.crispr.model;

import java.util.Date;



public class Experiment
{
    private Integer id;
    private String animalName;
    private String assayId;
    private String assayName;
    private String experimenter;
    private Date experimentDate;
    public Experiment(int id, String animalName, String assayId, String assayName, String experimenter)
    {
        super();
        this.id = id;
        this.animalName = animalName;
        this.assayId = assayId;
        this.assayName = assayName;
        this.experimenter = experimenter;
        this.experimentDate = new Date(generateDate());
        
    }
    public Integer getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
     
    
    
    public String getAnimalName()
    {
        return animalName;
    }
    public void setAnimalName(String animalName)
    {
        this.animalName = animalName;
    }
    public String getAssayId()
    {
        return assayId;
    }
    public void setAssayId(String assayId)
    {
        this.assayId = assayId;
    }
    public String getAssayName()
    {
        return assayName;
    }
    public void setAssayName(String assayName)
    {
        this.assayName = assayName;
    }
    public String getExperimenter()
    {
        return experimenter;
    }
    public void setExperimenter(String experimenter)
    {
        this.experimenter = experimenter;
    }
    public Date getExperimentDate()
    {
        return experimentDate;
    }
    public void setExperimentDate(Date experimentDate)
    {
        this.experimentDate = experimentDate;
    }
    public  long generateDate()
    {
        Date dt = new Date();       
        long dts =dt.getTime();       
        dts = (long) (dts - (Math.random() * 1) * 1530888898 -  (Math.random() * 1) *  1502031298);       
        return dts;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Experiment other = (Experiment) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }
}