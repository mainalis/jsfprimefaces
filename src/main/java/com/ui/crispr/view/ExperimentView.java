package com.ui.crispr.view;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.log4j.Logger;
import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

import com.ui.crispr.model.Experiment;
import com.ui.crispr.service.ExperimentService;

@ManagedBean(name="dtEditView")
@ViewScoped
public class ExperimentView implements Serializable
{
   
    private static final Logger logger = Logger.getLogger(ExperimentView.class);
    
    private static final long serialVersionUID = 1L;

    public List<Experiment> getExp1()
    {
        return exp1;
    }

    public void setExp1(List<Experiment> exp1)
    {
        this.exp1 = exp1;
    }

    public ExperimentService getExpService()
    {
        return expService;
    }

    public void setExpService(ExperimentService expService)
    {
        this.expService = expService;
    }

    private List<Experiment> exp1;
    
    @ManagedProperty("#{experimentService}")
    private ExperimentService expService;
    
    @PostConstruct
    public void init()
    {
        exp1 = expService.createExperiment();
    }
    
    public void deleteAction(Experiment ex)
    {
        //exp1.remove(ex);
        System.err.println("experiment " + ex.getId());
        System.err.println("removing experiment " +   exp1.remove(ex));
        logger.debug("removing experiment " +   exp1.remove(ex));
        
    }
    
    public void deleteAction(ActionEvent event)
    {
        ActionEvent ev = event;
        //System.err.println("removing experiment " +   exp1.remove(event));
        logger.debug("removing experiment " +   ev);
        logger.error("removing experiment " );

    }
    
    public void onRowEdit(RowEditEvent event)
    {
        FacesMessage msg = new FacesMessage("Experiment edited", ((Experiment)event.getObject()).getId()+ "");
        FacesContext.getCurrentInstance().addMessage(null, msg);       
    }
    
    public void onCellEdit(CellEditEvent event)
    {
        Object oldvalue = event.getOldValue();
        Object newValue = event.getNewValue();
        
        if(newValue != null && !newValue.equals(oldvalue)) {
            FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, 
                    "Cell Changed", "Old: " + oldvalue + ", New:" + newValue);
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
    }
    
    public void setService(ExperimentService service) {
        this.expService = service;
    }
}
