package com.ui.crispr.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.ui.crispr.model.Experiment;


@ManagedBean(name = "experimentService")
@ApplicationScoped
public class ExperimentService
{
    private List <Experiment> EXPERIMENT;
        
    public List<Experiment> createExperiment()
    {
        prepeareExeriment();
        return EXPERIMENT;
    }
    
    private  void prepeareExeriment()
    {
        String [] animalName = new String[] 
        {
                "B6NTAC-USA/774.1c",
                "CAB39L-TM1B-IC/8.1b",
                "FBXL21-TM2B-IC/12.1j",
                "PARD3-TM1B-IC/4.1a",
                "REXO2-TM1B-IC/4.1a",
                "GFI1B-TM1B-IC/9.1e",
                "ADAM23-TM1B-IC/7.1d",
                "VGF-DEL11-EM1-B6N-IC/1.2f",
                "NFIX-DEL20-EM1-B6N-IC/2.1a",
                "IRX3-DEL5-EM2-B6N-IC/9.2a"
        };
        
        String [] assayName = new String [] {
                "IMPC - Simplified IPGTT",
                "IMPC - Adult LacZ",
                "IMPC - Auditory Brain Stem Response",
                "IMPC - Eye (Slit Lamp and Opthalamoscope)",
                "IMPC - Body Composition - Dexa",
                "IMPC - Body Composition - Dexa",
                "IMPC - Calorimetry",
                "IMPC - ECHO",
                "IMPC - Histopathology",
                "Embroy - Histopathology"
        };
        
        String [] assayId = new String [] {
                "1011",
                "1012",
                "1013",
                "1014",
                "1015",
                "1016",
                "1017",
                "1018",
                "1019",
                "1020"
        };
        
        String [] experimenter = new String [] 
        {
                "Experimenter A",
                "Experimenter B",
                "Experimenter C",
                "Experimenter D",
                "Experimenter E",
                "Experimenter F",
                "Experimenter G",
                "Experimenter H",
                "Experimenter I",
                "Experimenter J"
        };
               
        Set<Date> dateSet = new HashSet<>();
        
        while(dateSet.size() != 10)
        {
            dateSet.add(new Date(generateDate()));
        }
                
        Set<Experiment> exp = new HashSet<>();
                
        Experiment ex = null;
        Random r1 = null;
        while(exp.size() != 500)
        {
            r1 = new Random();           
            ex = new Experiment(exp.size(), 
                    animalName[r1.nextInt(9)], 
                    assayId[r1.nextInt(9)], 
                    assayName[r1.nextInt(9)], 
                    experimenter[r1.nextInt(9)]);            
            exp.add(ex);
        }
        
        EXPERIMENT = new ArrayList<>(exp);                      
    }
    
    public  long generateDate()
    {
        Date dt = new Date();       
        long dts =dt.getTime();       
        dts = (long) (dts - (Math.random() * 1) * 1530888898 -  (Math.random() * 1) *  1502031298);       
        return dts;
    }
}
